#include <iostream>
#include <stdint.h>
#include <iomanip>
#include <string>

int main()
{
    std::string line = "Privet";
    std::cout << line << '\n';
    std::cout << line.length() << '\n';
    std::cout << line[0] << " " << line[line.length() - 1] << '\n';
}

